// Include the angular controller
require('plugins/alerte/controller/alerteController');
require('plugins/alerte/styles/alerte.css');
require('plugins/alerte/services/sendMail.js');
require('plugins/alerte/services/sendDb.js');
require('plugins/alerte/services/sendRapport.js');
// The provider function, which must return our new visualization type
function AlertProvider(Private) {
  var TemplateVisType = Private(require('ui/template_vis_type/template_vis_type'));
  // Include the Schemas class, which will be used to define schemas
  var Schemas = Private(require('ui/vis/schemas'));

  // Describe our visualization
  return new TemplateVisType({
    name: 'alerte', // The internal id of the visualization (must be unique)
    title: 'Alerte', // The title of the visualization, shown to the user
    description: 'Alerte visualization', // The description of this vis
    icon: 'fa-battery-half', // The font awesome icon of this visualization
    template: require('plugins/alerte/template/index.html'), // The template, that will be rendered for this visualization
    params: {
      defaults: {
        max: 600,
        min: 100,
        mail: 'kibana.talan@gmail.com',
        critique:50,
        sending:true,
        css:
        "table, th, td\n " +
        "{border: 1px solid black;border-collapse: collapse;}\n",

     template:
     "<table style='width:50%'>\n"+
        "<tr><th>Username</th><th>Consommation</th></tr>\n"+
        "<tr ng-repeat='x in response'>\n"+
        "<td>{{x._source.user_name}}</td>\n"+
        "<td>{{x._source.consommation}}</td>\n"+
        "</tr> </table>\n"
      },
      editor: require('plugins/alerte/template/index_param.html')
    },
    // Define the aggregation your visualization accepts
    schemas: new Schemas([
      {
        group: 'metrics',
        name: 'tagsize',
        title: 'Tagsize',
        min: 1,
        max: 1,
        aggFilter: ['sum', 'min', 'max']
      },
      {
        group: 'buckets',
        name: 'tags',
        title: 'Tags',
        min: 0,
        max: 1,
        aggFilter: ['terms']
      }
    ])
  });
}

require('ui/registry/vis_types').register(AlertProvider);
