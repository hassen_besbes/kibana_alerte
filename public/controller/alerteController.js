define(function (require) {
  var module = require('ui/modules').get('alerte');
  var initiale=initiale="Veuillez choisir une configuration.";
  var depassementT, succesT;
  var critique;
  var depassement


  module.controller('AlertController', function ($scope, $http, Private, service,serviceDb,sendRapport) {

    $scope.title="Dashboard"
    var send=0;
    var config=false;
    $http.post('../api/getData').then((response) => {
      initiale = response.data.initiale;
    succesT = response.data.succes;
    depassementT = response.data.depassement;
    critique=response.data.critique;



  });
    $scope.titlee = initiale;
    $scope.attente = true;
    $scope.ok = false;
    $scope.erreur = false;


    var email = {
      to: $scope.vis.params.mail,
      text: ""

    };

    var etat = 0;

    var filterManager = Private(require('ui/filter_manager'));
    var tabifyAggResponse = Private(require('ui/agg_response/tabify/tabify'));
    var metrics = $scope.metrics = [];
    $scope.myObj = {
      "color": "white",
      "background": "#03A9F4"

    }
    $scope.filter = function (tag) {
      // Add a new filter via the filter manager
      filterManager.add(
        // The field to filter for, we can get it from the config
        $scope.vis.aggs.bySchemaName['tags'][0].params.field,
        // The value to filter for, we will read out the bucket key from the tag
        tag.label,
        // Whether the filter is negated. If you want to create a negated filter pass '-' here
        null,
        // The index pattern for the filter
        $scope.vis.indexPattern.title
      );

    };
    $scope.processTableGroups = function (tableGroups) {
      tableGroups.tables.forEach(function (table) {
        table.columns.forEach(function (column, i) {
          var fieldFormatter = table.aggConfig(column).fieldFormatter();
          metrics[0] = {label: column.title, value: table.rows[0][i]};

        });
      });
    };

    $scope.$watch('esResponse', function (resp) {
      if (!resp) {
        $scope.tags = null;
        return;
      }
      if($scope.vis.params.title.length!=0) {
        var rapport = {
          message: "<style>" + $scope.vis.params.css + "</style>" + "<body ng-app='myApp' ng-controller='myCtrl'>" + $scope.vis.params.template + "</body><script src='https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js'></script>",
          to: $scope.vis.params.mail,
          title:$scope.vis.params.title
        };


        var pos = 1;
        email.to = $scope.vis.params.mail;
        email.text = "";


        $scope.title = $scope.vis.params.title;
        etat = 0;
        $scope.processTableGroups(tabifyAggResponse($scope.vis, resp));
        try {
          // Retrieve the id of the configured tags aggregation
          var tagsAggId = $scope.vis.aggs.bySchemaName['tags'][0].id;
          // Retrieve the metrics aggregation configured
          var metricsAgg = $scope.vis.aggs.bySchemaName['tagsize'][0];
          console.log(metricsAgg);
          // Get the buckets of that aggregation
          var buckets = resp.aggregations[tagsAggId].buckets;
          pos = 2;
          var min = Number.MAX_VALUE,
            max = -Number.MAX_VALUE;

          // Transform all buckets into tag objects
          $scope.tags = buckets.map(function (bucket) {
            // Use the getValue function of the aggregation to get the value of a bucket
            var value = metricsAgg.getValue(bucket);
            // Finding the minimum and maximum value of all buckets
            min = Math.min(min, value);
            max = Math.max(max, value);
            return {
              label: bucket.key,
              value: value
            };
          });


          $scope.tags = $scope.tags.map(function (tag) {


            return tag;
          });

          config = true;
          switch ($scope.vis.aggs.bySchemaName['tagsize'][0].type.name) {
            case "sum":

              for (var l = 0; l < $scope.tags.length; l++) {
                if ($scope.tags[l].value >= $scope.vis.params.max) {
                  etat = 1;
                  $scope.myObj = {

                    "background": "#F44336"

                  }
                  email.text += "\n La somme de consommation de l'utilisateur " + $scope.tags[l].label + " = " + $scope.tags[l].value + " est supérieur au seuil maximum= " + $scope.vis.params.max;
                  if(depassementT.indexOf("{{"+$scope.vis.aggs.bySchemaName['tagsize'][0].params.field.name+"}}") > -1) {
                    depassement= depassementT.replace("{{"+$scope.vis.aggs.bySchemaName['tagsize'][0].params.field.name+"}}", $scope.tags[l].value)
                  }

                  $scope.titlee = depassement;

                  serviceDb.sendDb($scope.tags[l].label, $scope.tags[l].value, $scope.vis.params.title);
                  $scope.ok = false;
                  $scope.erreur = true;


                }

                else {
                  if ($scope.tags[l].value < $scope.vis.params.min) {

                    etat = 1;
                    if(depassementT.indexOf("{{"+$scope.vis.aggs.bySchemaName['tagsize'][0].params.field.name+"}}") > -1) {
                      depassement= depassementT.replace("{{"+$scope.vis.aggs.bySchemaName['tagsize'][0].params.field.name+"}}", $scope.tags[l].value)
                    }
                    $scope.titlee = depassement;

                    $scope.myObj = {

                      "background": "#F44336"

                    }
                    $scope.ok = false;
                    $scope.erreur = true;
                    email.text += "\n La consommation de consommation l'utilisateur " + $scope.tags[l].label + " = " + $scope.tags[l].value + " est inférieure au seuil minimum= " + $scope.vis.params.min;
                    serviceDb.sendDb($scope.tags[l].label, $scope.tags[l].value, $scope.vis.params.title);

                  }
                  else {
                    if (($scope.tags[l].value < ($scope.vis.params.min + $scope.vis.params.critique)) || ($scope.tags[l].value >= ($scope.vis.params.max - $scope.vis.params.critique))) {
                      $scope.titlee = critique;
                      etat = 2;
                      $scope.myObj = {

                        "background": "#F4661B"

                      }
                      $scope.ok = false;
                      $scope.erreur = true;

                    }
                  }
                }


              }
              break;
            case "min":
              for (var l = 0; l < $scope.tags.length; l++) {

                if ($scope.tags[l].value < $scope.vis.params.min) {

                  etat = 1;
                  $scope.myObj = {

                    "background": "#F44336"

                  }
                  $scope.ok = false;
                  $scope.erreur = true;
                  if(depassementT.indexOf("{{"+$scope.vis.aggs.bySchemaName['tagsize'][0].params.field.name+"}}") > -1) {
                    depassement= depassementT.replace("{{"+$scope.vis.aggs.bySchemaName['tagsize'][0].params.field.name+"}}", $scope.tags[l].value)
                  }
                  $scope.titlee = depassement;
                  email.text += "\n La consommation de l'utilisateur " + $scope.tags[l].label + " = " + $scope.tags[l].value + " est inférieure au seuil minimum== " + $scope.vis.params.min;

                  serviceDb.sendDb($scope.tags[l].label, $scope.tags[l].value,  $scope.vis.params.title);
                }

                else {
                  if ($scope.tags[l].value < ($scope.vis.params.min + $scope.vis.params.critique)) {
                    $scope.titlee = critique;
                    etat = 2;
                    $scope.myObj = {

                      "background": "#F4661B"

                    }
                    $scope.ok = false;
                    $scope.erreur = true;

                  }
                }
              }
              break;
            case "max":
              for (var l = 0; l < $scope.tags.length; l++) {

                if ($scope.tags[l].value >= $scope.vis.params.max) {
                  etat = 1;
                  if(depassementT.indexOf("{{"+$scope.vis.aggs.bySchemaName['tagsize'][0].params.field.name+"}}") > -1) {
                    depassement= depassementT.replace("{{"+$scope.vis.aggs.bySchemaName['tagsize'][0].params.field.name+"}}", $scope.tags[l].value)
                  }
                  $scope.titlee = depassement;

                  $scope.myObj = {

                    "background": "#F44336"

                  }
                  $scope.ok = false;
                  $scope.erreur = true;
                  email.text += "\n La consommation de l'utilisateur " + $scope.tags[l].label + " = " + $scope.tags[l].value + " est supérieur au seuil maximum= " + $scope.vis.params.max;
                  serviceDb.sendDb($scope.tags[l].label, $scope.tags[l].value,  $scope.vis.params.title);

                }
                else {
                  if ($scope.tags[l].value >= ($scope.vis.params.max - $scope.vis.params.critique)) {
                    $scope.titlee = critique;
                    etat = 2;
                    $scope.myObj = {

                      "background": "#F4661B"

                    }
                    $scope.ok = false;
                    $scope.erreur = true;

                  }
                }

              }
              break;

          }


          if (etat == 0) {
            $scope.myObj = {
              "background": "#b2dba1"
            }
            $scope.erreur = false;
            $scope.attente = false;
            $scope.ok = true;
            $scope.titlee = succesT;

          }
          else {
            if ((etat == 1)&&($scope.vis.params.sending))
              service.sendMail(email);
          }
        }
        catch (err) {
        }
        if (pos == 1) {
          if (!metrics[0].label.indexOf("Max")) {
            config = true;
            if (metrics[0].value >= $scope.vis.params.max) {
              if(depassementT.indexOf("{{"+$scope.vis.aggs.bySchemaName['tagsize'][0].params.field.name+"}}") > -1) {
                depassement= depassementT.replace("{{"+$scope.vis.aggs.bySchemaName['tagsize'][0].params.field.name+"}}", metrics[0].value)
              }
              $scope.titlee = depassement;
              $scope.myObj = {

                "background": "#F44336",


              }
              $scope.ok = false;
              $scope.erreur = true;
              email.text = "\n Consommation= " + metrics[0].value + " est supérieur au seuil maximum= " + $scope.vis.params.max;
             if($scope.vis.params.sending)
             { service.sendMail(email);}
              serviceDb.sendDb("user", metrics[0].value, $scope.vis.params.title);


            }
            else {
              if (metrics[0].value >= ($scope.vis.params.max - $scope.vis.params.critique)) {
                $scope.titlee = critique;

                $scope.myObj = {

                  "background": "#F4661B"

                }
                $scope.ok = false;
                $scope.erreur = true;

              }

              else {
                $scope.myObj = {

                  "background": "#b2dba1"

                }
                $scope.erreur = false;
                $scope.attente = false;
                $scope.ok = true;
                $scope.titlee = succesT;

              }
            }
          }
          else {
            if (!metrics[0].label.indexOf("Min")) {
              config = true;
              if (metrics[0].value <= $scope.vis.params.min) {

                etat = 1;
                $scope.myObj = {

                  "background": "#F44336"

                }
                email.text = "\n Consommation= " + metrics[0].value + " est inférieure  au seuil minimum= " + $scope.vis.params.min;
                if($scope.vis.params.sending)
                { service.sendMail(email);}
                if(depassementT.indexOf("{{"+$scope.vis.aggs.bySchemaName['tagsize'][0].params.field.name+"}}") > -1) {
                  depassement= depassementT.replace("{{"+$scope.vis.aggs.bySchemaName['tagsize'][0].params.field.name+"}}", metrics[0].value)
                }

                $scope.titlee = depassement;
                $scope.ok = false;
                $scope.erreur = true;
                serviceDb.sendDb("user", metrics[0].value,  $scope.vis.params.title);

              }
              else {
                if (metrics[0].value < ($scope.vis.params.min + $scope.vis.params.critique)) {
                  $scope.titlee = critique;
                  etat = 2;
                  $scope.myObj = {

                    "background": "#F4661B"

                  }
                  $scope.ok = false;
                  $scope.erreur = true;

                }

                else {
                  $scope.myObj = {

                    "background": "#b2dba1"

                  }
                  $scope.erreur = false;
                  $scope.attente = false;
                  $scope.ok = true;
                  $scope.titlee = succesT;

                }
              }
            }
            else {

              if (!metrics[0].label.indexOf("Sum")) {
                config = true;

                if (metrics[0].value <= $scope.vis.params.min) {

                  $scope.myObj = {

                    "background": "#F44336"

                  }

                  email.text = "\n La somme de consommation= " + metrics[0].value + " est inférieure  au seuil minimum= " + $scope.vis.params.min;
                  serviceDb.sendDb("user", metrics[0].value,  $scope.vis.params.title);
                  if($scope.vis.params.sending)
                  { service.sendMail(email);}
                  if(depassementT.indexOf("{{"+$scope.vis.aggs.bySchemaName['tagsize'][0].params.field.name+"}}") > -1) {
                    depassement= depassementT.replace("{{"+$scope.vis.aggs.bySchemaName['tagsize'][0].params.field.name+"}}", metrics[0].value)
                  }

                  $scope.titlee = depassement;

                  $scope.ok = false;
                  $scope.erreur = true;

                }
                else {
                  if (metrics[0].value >= $scope.vis.params.max) {

                    $scope.myObj = {

                      "background": "#F44336"

                    }
                    if(depassementT.indexOf("{{"+$scope.vis.aggs.bySchemaName['tagsize'][0].params.field.name+"}}") > -1) {
                      depassement= depassementT.replace("{{"+$scope.vis.aggs.bySchemaName['tagsize'][0].params.field.name+"}}", metrics[0].value)
                    }
                    $scope.titlee = depassement;
                    serviceDb.sendDb("user", metrics[0].value,  $scope.vis.params.title);
                    $scope.ok = false;
                    $scope.erreur = true;
                    email.text = "\n La somme de consommation= " + metrics[0].value + " est supérieur au seuil maximum= " + $scope.vis.params.max;

                    if($scope.vis.params.sending)
                    { service.sendMail(email);}


                  }
                  else {
                    if ((metrics[0].value < ($scope.vis.params.min + $scope.vis.params.critique)) || (metrics[0].value >= ($scope.vis.params.max - $scope.vis.params.critique))) {
                      $scope.titlee = critique;

                      $scope.myObj = {

                        "background": "#F4661B"

                      }
                      $scope.ok = false;
                      $scope.erreur = true;

                    }


                    else {
                      $scope.myObj = {

                        "background": "#b2dba1"

                      }
                      $scope.erreur = false;
                      $scope.attente = false;
                      $scope.ok = true;
                      $scope.titlee = succesT;

                    }
                  }
                }

              }
            }
          }
        }
        if ((send == 0) && (config == true)) {
          if($scope.vis.params.sending)
          {sendRapport.sendRapport(rapport);
            send = 1;}


        }
      }
    });


  });
});
