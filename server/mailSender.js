export function send(email) {
  var fs = require('fs');
  /*
   var pdf = require('html-pdf');
   var html ="<style>table, th, td {border: 1px solid black;border-collapse: collapse;}</style><table style='width:50%'><tr><th>Username</th><th>Consommation</th></tr><tr><td>Jill</td><td>Smith</td></tr></table>";
   var options = { format: 'Letter' };

   pdf.create(html, options).toFile('./businesscard.pdf', function(err, res) {
   if (err) return console.log(err);
   console.log(res); // { filename: '/app/businesscard.pdf' }
   });
   */
  var nodemailer = require('nodemailer');


  let transporter = nodemailer.createTransport({
    service: email.service,
    auth: {

      user: email.from,
      pass: email.password
    },
    tls: {
      rejectUnauthorized: false
    }

  });
  let HelperOptions = {
    from: email.from,
    to: email.to,
    subject: email.subject,
    text: email.text/*,
    attachments: [{'filename': 'businesscard.pdf', 'path': 'businesscard.pdf'}]*/

  };


  transporter.sendMail(HelperOptions, function (error, info) {
    if (error) {
      return console.log(error);
    } else {
      console.log('Email sent: ' + info.response);

    }
  });
}
