export function deleteDb(name) {
  var elasticsearch = require('elasticsearch');
  var client = new elasticsearch.Client({
    host: 'localhost:9200',
    log: 'trace'
  });

  client.indices.delete({
    index: name,
    ignore: [404]
  }).then(function (body) {
    // since we told the client to ignore 404 errors, the
    // promise is resolved even if the index does not exist
    console.log('index was deleted or never existed');
  }, function (error) {
    // oh no!
  });
}
