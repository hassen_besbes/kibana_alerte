export function email(server) {


  var mailSender = require('./mailSender.js');
  server.route({
    method: 'POST',
    path: '/api/email',
    handler: function (request, reply) {
      const config = server.config();

      var data = JSON.parse(JSON.stringify(request.payload));
      var email = {
        from: config.get('mail.from'),
		    password:config.get('mail.password'),
        to: data.to,
        subject: config.get('mail.subject'),
        service: config.get('mail.service'),
        text: config.get('mail.message') + "\n" + data.text
      }

      mailSender.send(email);

      reply('succes!');

    }
  });

};
export function getData(server) {
  server.route({
    method: 'POST',
    path: '/api/getData',
    handler: function (request, reply) {
      const config = server.config();
      var response = {
        depassement: config.get('message.depassement'),
        succes: config.get('message.succes'),
        initiale: config.get('message.initiale'),
        critique: config.get('message.critique')
      };


      reply(response);
      console.log('succes!');

    }
  });

};
export function sendDb(server) {
 var dbSender = require('./dbSender.js');

  server.route({
    method: 'POST',
    path: '/api/sendDb',
    handler: function (request, reply) {
      var data = JSON.parse(JSON.stringify(request.payload));
      var param ={
        username:data.user,
        consommation: data.consommation,
        index: "kibana_"+data.title
      };

      dbSender.send(param);

      reply(param);
  }
  });

};


export function sendRapport(server) {
  server.route({
    method: 'POST',
    path: '/api/sendRapport',
    handler: function (request, reply) {

      var schedule = require('node-schedule');
      var sleep = require('system-sleep');
      const config = server.config();
      var data = JSON.parse(JSON.stringify(request.payload));
      var rapportCreator = require('./rapportCreator.js');

      var email = {
        from: config.get('mail.from'),
        password:config.get('mail.password'),
        to: data.to,
        subject: config.get('mail.subject_report'),
        service: config.get('mail.service'),
      };
      var rapportSender = require('./rapportSender.js');
      var deleteDb=require('./dbDeleter.js');
      var temps = schedule.scheduleJob(config.get('mail.time'), function(){
        try{
        rapportCreator.create(data.message,"kibana_"+data.title);
        sleep(20000);
        rapportSender.send(email);
        deleteDb.deleteDb("kibana_"+data.title);
        }
        catch(e)
        { console.log('error!');

        }
      });

      reply('succes!');
      console.log('succes!');
    }
  }
  );


};
