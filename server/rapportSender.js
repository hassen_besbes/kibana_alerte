export function send(email) {
  var fs = require('fs');

  var nodemailer = require('nodemailer');


  let transporter = nodemailer.createTransport({
    service: email.service,
    auth: {

      user: email.from,
      pass: email.password
    },
    tls: {
      rejectUnauthorized: false
    }

  });
  let HelperOptions = {
    from: email.from,
    to: email.to,
    subject: email.subject,
    text: "",
     attachments: [{'filename': 'rapport.pdf', 'path': 'rapport.pdf'}]

  };


  transporter.sendMail(HelperOptions, function (error, info) {
    if (error) {
      return console.log(error);
    } else {
      console.log('Email sent: ' + info.response);

    }
  });
}
