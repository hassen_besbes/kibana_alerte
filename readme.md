mail.from : kibana.talan@gmail.com
mail.password: azerty@@AZERTY
mail.service: gmail
mail.subject: Dépassement du seuil
mail.message: Attention le dépassement du seuil a été atteint.
mail.subject_report: Rapport de depassement.
mail.time: 29 02 * * *
message.depassement: Un dépassement du seuil a été atteint valeur:{{consumption}}
message.succes: Aucun dépassement détecté.
message.initiale: Veuillez choisir une configuration.
message.critique: Attention vous êtes dans la zone critique.




Please add this in kibana/src/server/config/schema.js:

  mail: Joi.object({
    allowAnonymous: Joi.boolean().default(true),
    from:Joi.string(),
	password:Joi.string(),
	service:Joi.string(),
	subject:Joi.string(),
	message:Joi.string(),
	subject_report:Joi.string(),
	time:Joi.string()
  }).default(),
 message: Joi.object({
    allowAnonymous: Joi.boolean().default(true),
    depassement:Joi.string(),
	succes:Joi.string(),
	initiale:Joi.string(),
	critique:Joi.string()
  }).default(),