var api = require('./server/routes.js');

var response = require('./server/routes.js');
module.exports = function (kibana) {
  return new kibana.Plugin({
    uiExports: {
      visTypes: ['plugins/alerte/alerte']
    },


    init(server, options) {
      api.getData(server);
      api.email(server);

	  api.sendDb(server);
//      api.createRapport(server);
      api.sendRapport(server);

    }

  });

};
